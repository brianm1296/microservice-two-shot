import React, {useEffect, useState} from "react";

function HatsList() {
    const [ hats, setHats] = useState([])
    // async function loadHats()
    const loadHats = async () => {
        const hatsURL = 'http://localhost:8090/api/hats/'
        const hats_response = await fetch(hatsURL)
        if(!hats_response.ok){
            console.error('Hats loading error')
        } else {
            const hats_data = await hats_response.json()
            // console.log(hats_data)
            setHats(hats_data.hats)
    }
  }
    const deleteHat = (id) => async() => {
        const hatsURL = `http://localhost:8090/api/hats/${id}/`
        const fetchConfig = {
            method: 'delete',
        }
        const hats_response = await fetch(hatsURL, fetchConfig)
        if(!hats_response.ok){
            console.error('Hat deletion error')
        } else {
            loadHats()
        }
  }

    useEffect(() => {
            loadHats()
            },[])


    return (
    <div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return(
                        <tr key={hat.id}>
                            <td>{hat.style_name}</td>
                            <td>{hat.fabric}</td>
                            <td>{hat.color}</td>
                            <td>{hat.location.closet_name}</td>
                            <td><button onClick={deleteHat(hat.id)}className='btn btn-dark'>Delete</button></td>
                        </tr>
                            )
                        }
                    )
                   }
            </tbody>
        </table>
    </div>
    )
}

export default HatsList
