import React, {useEffect, useState} from "react";

function ShoesList() {
    const [shoes, setShoes] = useState([]);
    const getShoes = async () => {
      const shoesUrl = `http://localhost:8080/api/shoes/`;
      const response = await fetch(shoesUrl);
      if (response.ok) {
        const listShoes = await response.json();
        setShoes(listShoes.shoes);
      }
    };

    useEffect(() => {
      getShoes();
    }, []);

    const deleteShoe = (id) => async () => {
      try {
        const url = `http://localhost:8080/api/shoes/${id}/`;
        const deleteResponse = await fetch(url, {
          method: "delete",
        });

        if (deleteResponse.ok) {
          const reloadUrl = `http://localhost:8080/api/shoes/`;
          const reloadResponse = await fetch(reloadUrl);
          const newShoes = await reloadResponse.json();
          setShoes(newShoes.shoes);
        }
      } catch (err) {}
    };

    if (shoes === undefined) {
      return null;
    }

    return (
      <>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Model Name</th>
              <th>Color</th>
              <th>Photo</th>
              <th>Bin</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map((shoe) => {
              return (
                <tr key={shoe.id}>
                  <td>{shoe.manufacturer}</td>
                  <td>{shoe.name}</td>
                  <td>{shoe.color}</td>
                  <td>
                    <img
                      src={shoe.photo}
                      alt=""
                      width="100px"
                      height="100px"
                    />
                  </td>
                  <td>{shoe.bin.bin_number}</td>
                  <td>
                    <button onClick={deleteShoe(shoe.id)} className="btn btn-danger">
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
  }

  export default ShoesList;
