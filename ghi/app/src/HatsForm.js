import React, { useEffect, useState } from "react";

function HatsForm() {

    const [ style, setStyle] = useState('')
    function handleStyle(e){
        const value = e.target.value
        setStyle(value)
    }

    const [ color, setColor] = useState('')
    function handleColor(e){
        const value = e.target.value
        setColor(value)
    }

    const [ fabric, setFabric] = useState('')
    function handleFabric(e){
        const value = e.target.value
        setFabric(value)
    }

    const [ picture, setPicture] = useState('')
    function handlePicture(e){
        const value = e.target.value
        setPicture(value)
    }

    const [ location, setLocation] = useState('')
    function handleLocation(e){
        const value = e.target.value
        setLocation(value)
    }

    const [ locations, setLocations] = useState([])

    const fetchLocations = async () => {
        const locationsURL = 'http://localhost:8100/api/locations/'
        const response = await fetch(locationsURL)
        if(!response.ok){
            console.error('Failed to get location list')
        } else {
            const data = await response.json()
            setLocations(data.locations)
            console.log(data.locations)
        }
    }


    async function handleSubmit(e) {

        e.preventDefault()

        const data = {}
        data.fabric = fabric
        data.style_name = style
        data.color = color
        data.picture_url = picture
        data.location = location

        console.log(data)

        const hatURL = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method:'post',
            body:JSON.stringify(data),
            headers: {
                'Content-Type' : 'application/json',
            }
        }

        const response = await fetch(hatURL, fetchConfig)
        if(response.ok){
            const newHat = await response.json()

            setColor('')
            setFabric('')
            setLocation('')
            setPicture('')
            setStyle('')

            const successTag=document.getElementById('created')
            successTag.classList.remove('d-none')
        }
    }

    useEffect(() =>{fetchLocations()},[])


    return (
    <div className="row">
    <div className='offset-3 col-6'>
    <div className='shadow p-4 mt-4'>
        <h1>Create a new hat</h1>
    <form onSubmit={handleSubmit} id='create-hat-form'>
        <div>
        <label htmlFor='style'>Style:</label>
            <input onChange={handleStyle} value={style} placeholder="Style" required type="text" name="style" id="style" className="form-control"></input>
        </div>
        <div>
        <label htmlFor='color'>Color:</label>
            <input onChange={handleColor} value={color} placeholder="Color" required type="text" name='color' id='color' className="form-control"></input>

        </div>
        <div>
        <label htmlFor='fabric'>Fabric:</label>
            <input onChange={handleFabric} value={fabric} placeholder="Fabric" required type="text" name='fabric' id='fabric' className="form-control"></input>

        </div>
        <div>
        <label htmlFor='picture'>Picture URL:</label>
            <input onChange={handlePicture} value={picture} placeholder="Picture URL" required type='url' name='picture' id='picture' className='form-control'></input>

        </div>
        <div>
            <label></label>
            <select onChange={handleLocation} value={location} required id='location' name='location' className="form-select">
                <option value=''>Choose a closet location</option>
                {locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>{location.closet_name}</option>
                    )
                })}

            </select>
        </div>
        <div className='mt-3 mb-3'>
        <button className="btn  btn-dark">Create</button>
        </div>
        <div className='text-center alert alert-success mb-0 d-none' id='created'>
            New hat has been created
        </div>
    </form>
    </div>
    </div>
    </div>
    )
}

export default HatsForm
