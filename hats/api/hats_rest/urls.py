from django.urls import path
from .views import list_hats, details_hat

urlpatterns = [
    path('hats/', list_hats, name="list_hats"),
    path('hats/<int:id>/', details_hat, name="details_hat")
]
