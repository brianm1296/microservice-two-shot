from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Hat, LocationVO


class LocationVOEncoder(ModelEncoder):
     model = LocationVO
     properties = [
          'import_id',
          'closet_name',
          'section_number',
          'shelf_number',
     ]


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        'fabric',
        'style_name',
        'color',
        'picture_url',
        'location',
        'id',
    ]

    encoders = {
         'location': LocationVOEncoder()
    }

@require_http_methods(['GET','POST'])
def list_hats(request):
    if request.method == 'GET':
        hats = Hat.objects.all()
        return JsonResponse({'hats':hats}, encoder=HatEncoder, safe=False)
    else:
        body = json.loads(request.body)
        try:
            location_id = body['location']
            location = LocationVO.objects.get(import_id=location_id)
            body['location'] =  location
        except LocationVO.DoesNotExist:
             return JsonResponse({'message': 'Location Error'}, status=400)

        hat = Hat.objects.create(**body)
        return JsonResponse(hat, encoder=HatEncoder, safe=False)


@require_http_methods(['GET','PUT','DELETE'])
def details_hat(request, id):
    if request.method == 'GET':
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(hat, encoder=HatEncoder, safe=False)

        except Hat.DoesNotExist:
                return JsonResponse({'message':'Hat ID does not exist'}, status=400)

    elif request.method == 'PUT':
        try:
            body = json.loads(request.body)
            try:
                location_id = body['location']
                location = LocationVO.objects.get(import_id=location_id)
                body['location'] =  location
            except LocationVO.DoesNotExist:
                return JsonResponse({'message': 'Location Error'}, status=400)
            Hat.objects.filter(id=id).update(**body)
            hat = Hat.objects.get(id=id)
            return JsonResponse(hat, encoder=HatEncoder, safe=False)

        except Hat.DoesNotExist:
                return JsonResponse({'message':'Hat ID does not exist'}, status=400)
    elif request.method == 'DELETE':
         try:
            count, _ = Hat.objects.get(id=id).delete()
            return JsonResponse({'deleted': count > 0})
         except Hat.DoesNotExist:
              return JsonResponse({'message':'Hat ID does not exist'}, status=400)
