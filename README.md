# Wardrobify

Team project

* Felix - shoes
* Brian Miller - hats

## Design

## Shoes microservice

The Shoe and BinVO model was created and there can be multiple shoes for one bin.
Since the bin data is stored in the wardrobe micro-service, the shoes microservice should not be able to make changes to the bin data. Therefore, a polling microservice was made to create instances of the BinVO for the shoe microservice.
The models and wardrobe microservice was integrated together to create an application using React by implementing RESTful apis for getting, creating, and deleting shoes.

## Hats microservice

I made the Hat and LocationVO models. The poller uses the models to add new hats to the wardrobe and associating them to a specific location. You can then create new hats, read or delete existing entries based on the criteria of the hat.
